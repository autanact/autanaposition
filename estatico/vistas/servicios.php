<div id='marca_servicios'></div>
<div id='servicios'>
	<br><br><br><br>
	<div id="contenedor0servicio">		
			Ofrecemos la soluci&oacute;n para el Monitoreo y Rastreo de sus Activos M&oacute;viles y Seres Queridos
	</div>
	<div id="contenedor1servicio">	
		<div id="servicio1slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>transporte_publico_1.jpg"></div>
		
		<div id="servicio1slides1">
			 <div>
			 	<div class="mensajeservicio3 animacion">Env&iacute;e el veh&iacute;culo m&aacute;s cercano</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_publico_1.jpg">
			 </div>
			 
			  <div>
			 	<div class="mensajeservicio3 animacion">Controle el exceso de velocidad</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_publico_2.jpg">
			 </div>
			 
			  <div>
			 	<div class="mensajeservicio3 animacion">Reciba Alerta por fuera de Ruta</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_publico_3.jpg">
			 </div>
		</div>
	</div>
	
	<div id="contenedor2servicio">
		<div id="servicio2slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>transporte_especial_1.jpg"></div>
		
		<div id="servicio2slides1">
			 <div>
			 	<div class="mensajeservicio3 animacion">Ofrezca mayor seguridad a sus pasajeros</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_especial_1.jpg">
			 </div>
			 
			  <div>
			 	<div class="mensajeservicio3 animacion">D&eacute; el mejor servicio</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_especial_2.jpg">
			 </div>
			 
			  <div>
			 	<div class="mensajeservicio3 animacion">Mantenga actualizada la informaci&oacute;n del conductor</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_especial_3.jpg">
			 </div>
			 
			 <div>
			 	<div class="mensajeservicio3 animacion">Controle toda su flota</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_especial_4.jpg">
			 </div>
		</div>
	</div>
	
	<div id="contenedor3servicio">
		
		<div id="servicio3slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>transporte_carga_1.jpg"></div>
		
		<div id="servicio3slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Reduzca el consumo de Gasolina</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>transporte_carga_1.jpg">
			  </div>
			
			  <div>
			  	<div class="mensajeservicio3 animacion">Controle la temperatura de su mercanc&iacute;a</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_carga_2.jpg">
			  </div> 	
		</div>
	</div>
	
	<div id="contenedor4servicio">
		<div id="servicio4slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>transporte_servicio_1.jpg"></div>
		
		<div id="servicio4slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Optimice los procesos log&iacute;sticos</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>transporte_servicio_1.jpg">
			  </div>
			
			  <div>
			  	<div class="mensajeservicio3 animacion">Reduzca los costos operativos</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_servicio_2.jpg">
			  </div> 	
		</div>
	</div>
	
	
	<div id="tituloservicio1">
		<div class="lineaservicios1"></div>
		Transporte P&uacute;blico<br>
		<div class="sombratitulo1"></div>
	</div>
	
	<div id="tituloservicio2">
		<div class="lineaservicios2"></div>
		Transporte Especial<br>
		<div class="sombratitulo2"></div>
	</div>

	<div id="tituloservicio3">
		<div class="lineaservicios3"></div>
		Transporte de Carga<br>
		<div class="sombratitulo3"></div>
	</div>

	<div id="tituloservicio4">
		<div class="lineaservicios4"></div>
		Transporte de Servicio<br>
		<div class="sombratitulo4"></div>
	</div>
	
	<div id="contenedor5servicio">
		<div id="servicio5slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>transporte_sanitario_1.jpg"></div>
		
		<div id="servicio5slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Verifique el recorrido de su veh&iacute;culo</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>transporte_sanitario_1.jpg">
			  </div>
			
			  <div>
			  	<div class="mensajeservicio3 animacion">Indentifique en el plano las Cl&iacute;nicas y Hospitales</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_sanitario_2.jpg">
			  </div> 
			  
			  <div>
			  	<div class="mensajeservicio3 animacion">Trace la ruta m&aacute;s &oacute;ptima</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>transporte_sanitario_3.jpg">
			  </div> 	
		</div>
	</div>
	
	<div id="contenedor6servicio">
		<div id="servicio6slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>motos_1.jpg"></div>
		
		<div id="servicio6slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Realice su entrega a tiempo</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>motos_1.jpg">
			  </div>
			
			  <div>
			  	<div class="mensajeservicio3 animacion">Incremente la Seguridad de su activo m&oacute;vil</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>motos_2.jpg">
			  </div> 
		</div>
	</div>
	
	<div id="contenedor7servicio">
		<div id="servicio7slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>personas_1.jpg"></div>
		
		<div id="servicio7slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Dele protecci&oacute;n a sus seres queridos</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>personas_1.jpg">
			  </div>
			
			  <div>
			  	 <div class="mensajeservicio3 animacion">Controle la ubicaci&oacute;n de sus ni&#241;os</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>personas_2.jpg">
			  </div> 
			  
			   <div>
			  	 <div class="mensajeservicio3 animacion">y Adolescente</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>personas_3.jpg">
			  </div> 
		</div>
	</div>
	<div id="contenedor8servicio">
		<div id="servicio8slides2" ><img src="<?php print(DIR_WS_IMAGENES) ?>mascotas_1.jpg"></div>
		
		<div id="servicio8slides1">
			  <div>
			 	 <div class="mensajeservicio3 animacion">Localiza r&aacute;pidamente a t&uacute; mascota</div>
			 	 <img src="<?php print(DIR_WS_IMAGENES) ?>mascotas_1.jpg">
			  </div>
			
			  <div>
			  	<div class="mensajeservicio3 animacion">Reciba una alerta cuando est&eacute; fuera de zona</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>mascotas_2.jpg">
			  </div> 
			  
			  <div>
			  	<div class="mensajeservicio3 animacion">Aseg&uacute;rate de que t&uacute; mascota nunca se pierda</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>mascotas_3.jpg">
			  </div> 
			  
			  <div>
			  	<div class="mensajeservicio3 animacion">Sabr&aacute;s en todo momento donde se encuentra</div>
			 	<img src="<?php print(DIR_WS_IMAGENES) ?>mascotas_4.jpg">
			  </div> 
		</div>
	</div>
	
	<div id="tituloservicio5">
		<div class="lineaservicios5"></div>
		Transporte Sanitario<br>
		<div class="sombratitulo5"></div>
	</div>

	<div id="tituloservicio6">
		<div class="lineaservicios6"></div>
		Motos<br>
		<div class="sombratitulo6"></div>
	</div>

	<div id="tituloservicio7">
		<div class="lineaservicios7"></div>
		Personas<br>
		<div class="sombratitulo7"></div>
	</div>

	<div id="tituloservicio8">
		<div class="lineaservicios8"></div>
		Mascotas<br>
		<div class="sombratitulo8"></div>
	</div>
</div>
<script>
	$(function() {
		//SE ACTIVAN LOS OCHO SLIDES DE LAS 8 CAJAS DE LA PAGINA
	    $('#servicio1slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio2slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio3slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio4slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio5slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio6slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio7slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
	    $('#servicio8slides1').slidesjs({width: 0, height: 0, play: {active: false,auto: true, interval: 5000, swap: false }, navigation: {active: false}, pagination: {active: false}});
    });
</script>      
