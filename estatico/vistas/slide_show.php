<div id="container" >
	<div id="slides" >
	  <div>
	  		<div class="mensajebanner1">
	  		Realice el control de su flota de veh&iacute;culos en forma r&aacute;pida, sencilla y eficaz. Autana Position le ofrece la soluci&oacute;n
			</div>
	  		<img src="<?php print(DIR_WS_IMAGENES) ?>banner3.jpg">
	  </div>
	  
	  <div>
	  		<div class="mensajebanner1">
	  		Le ofrecemos nuestra asesor&iacute;a permanente para que obtenga todos los beneficios de la plataforma
			</div>
	  		<img src="<?php print(DIR_WS_IMAGENES) ?>banner1.jpg">
	  </div>
	  
	  <div>
	  		<div class="mensajebanner1">
	  		Ya est&aacute; listo el  m&oacute;dulo  para el mantenimiento de sus veh&iacute;culos para  que administre la informaci&oacute;n relevante de las revisiones, servicios y reparaciones
			</div>
	  		<img src="<?php print(DIR_WS_IMAGENES) ?>banner2.jpg">
	  </div>
	  
	  <div>
	  		<div class="mensajebanner1">
	  		Gran variedad de &iacute;conos para una mejor identificaci&oacute;n de sus activos m&oacute;viles y sitios de inter&eacute;s
			</div>
	  		<img src="<?php print(DIR_WS_IMAGENES) ?>banner8.jpg">
	  </div>

	  <div>
	  		<div class="mensajebanner1">
	  		Con el dispositivo port&aacute;til GPS podr&aacute; proteger a su familia, desde su celular, tablet o PC
			</div>
	  		<img src="<?php print(DIR_WS_IMAGENES) ?>banner9.jpg">
	  </div>
	  
      <a href="#" class="slidesjs-previous slidesjs-navigation"></a>
      <a href="#" class="slidesjs-next slidesjs-navigation"></a>
	</div>
</div>
  
<script>
    $(function() {
      $('#slides').slidesjs({
        width: 0,
        height: 0,
		navigation: {
			active: false,
			swap: false,
			// [boolean] Generates next and previous buttons.
			// You can set to false and use your own buttons.
			// User defined buttons must have the following:
			//previous button: class="slidesjs-previous slidesjs-navigation",
			//next button: class="slidesjs-next slidesjs-navigation",
			effect: "slide"
			// [string] Can be either "slide" or "fade".	
		},
        play: {
          active: false,
          auto: true,
          interval: 5000,
          swap: false, 
          pauseOnHover: true
        }, 
		pagination: {
		  active: true,
			// [boolean] Create pagination items.
			// You cannot use your own pagination. Sorry.
		  effect: "slide"
			// [string] Can be either "slide" or "fade".
		}
      });
    });
</script>