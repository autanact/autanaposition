			</div>  <!--SE CIERRA EL CONTENEDOR CENTRAL -->
			<div id='foot'>
				<div id="contenedor1foot">
					<div id="logofoot">
						<center><img src="<?php echo DIR_WS_IMAGENES."logoautanaposition_grande.png" ?>"></center>
					</div>
					
					Autana Position empresa de tecnolog&iacute;a de vanguardia que  ofrece  la soluci&oacute;n para el control de su flota, 
					suministr&aacute;ndole todos los productos y servicios necesarios, d&aacute;ndole especial inter&eacute;s al asesoramiento a 
					nuestros clientes para que explote toda la potencialidad del servicio.
					Esta plataforma se adapta a cada industria y a todo tipo de empresa con veh&iacute;culos o activos que necesiten ser monitoreados.
					<br>
				</div>
				<div id="contenedor2foot">
					<center><p>Nosotros</p></center>
					Nuestra Misi&oacute;n es  ofrecer una plataforma de Monitoreo, Rastreo y Localizaci&oacute;n Satelital 
					con una amplia gama de dispositivos y servicios para Activos M&oacute;viles, Personas y Animales.
					<br><br>
					Nuestra visi&oacute;n crecer de manera estructurada e ilimitada, manteni&eacute;ndonos a la Vanguardia con Tecnolog&iacute;a de avanzada
					 y el personal m&aacute;s capacitado para brindar excelencia y calidad en nuestros productos y servicios.
					<br>
				</div>
				<div id="contenedor3foot">
					<p>Ubicaci&oacute;n</p>
					
					<script type='text/javascript'> 
						function init_map(){
							var myOptions = {
									zoom:15,
									center:new google.maps.LatLng(6.157995, -75.608769),
									mapTypeId: google.maps.MapTypeId.ROADMAP};
							        map = new google.maps.Map(document.getElementById('mapfoot'), 
								    myOptions);
					
						 		marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(6.156961, -75.608574)});
						 		infowindow = new google.maps.InfoWindow({content:'<strong>Sabaneta</strong><br>Sabaneta, Antioquia<br>'});
					
						 		google.maps.event.addListener(marker, 'click', 
						 		 		function(){
					 		 				infowindow.open(map,marker);
						 		});
					
						 infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
				 	</script>      
					
					<div id="mapfoot"></div>  
				</div>
				<div id="contenedor4foot">
					<center><p>Contactos</p></center>

				    E-mail: info@autanaposition.com  
					<br><br>
					Tel&eacute;fonos: <br> 
					+57 4   487.21.98 <br>
					+57 300 792.29.92 <br>
					<br>
					Cr 44 # 60 Sur - 35, Medell&iacute;n, Antioquia, Colombia.
					<br>
					 
				</div>
				<div id="contenedor5foot"> 2016 Autana Tecnolog&iacute;a de Vanguardia NIT: 900.965.683-6</div>
			</div>
	</body>
</HTML>
