<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>AUTANA POSITION</title>
	<meta name="AUTANA POSITION" content=""/>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<?php
	/*
	 * Created on 15/06/2009
	 *
	 * To change the template for this generated file go to
	 * Window - Preferences - PHPeclipse - PHP - Code Templates
	 */
	if (file_exists('../conf.php')){
		include('../conf.php');
	}else{
		include('plantillas/conf.php');
	}
	
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."estilos.css' type='text/css'/>");
	?>
	
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.slides.min.js"></script>	 
	<!-- SCRIPT PARA EL MAPA DE GOOGLE -->
	<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
	
	<link rel="icon" type="image/png" href="<?php print(DIR_WS_IMAGENES) ?>logoautanaposition.png" />
</head>
<body>
	<div id='head'>
		<div id="menu">
			<div class="sombratopmenu">&nbsp;</div>
			<div id="logo">
				<a id="inicio" href="#head"><img src="<?php print(DIR_WS_IMAGENES) ?>logoautanaposition.png"></a>
			</div>
			<div id="texto_menu">
				<a id="inicio" href="#marca_servicios">Productos y Servicios</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="http://mapas.autanaposition.com/login" target="_blank">Acceso a la Plataforma</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
			<div class="sombrafootmenu"></div>
		</div>  
		
	</div>	
	<div id="banner">
			<?php include_once(DIR_WS_ESTATICO."slide_show.php");?>
	</div>
	<div id='contenedor_central'> 